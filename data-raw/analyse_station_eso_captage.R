
# librairies ----------
library(DBI)
library(RPostgreSQL)
library(dplyr)
library(tidyverse)
library(glue)
library(sf)


# connection au SGBD et chargements-----------

drv <- dbDriver("PostgreSQL")

con_si_eau <- dbConnect(drv,
                        dbname="si_eau",
                        host=Sys.getenv("server"),
                        port=Sys.getenv("port"),
                        user=Sys.getenv('userid'),
                        password=Sys.getenv("sgbd_pwd"))
postgresqlpqExec(con_si_eau, "SET client_encoding = 'windows-1252'")

dsn_si_eau<-glue("PG:host={Sys.getenv('server')} port={Sys.getenv('port')} dbname='si_eau' user={Sys.getenv('userid')} password={Sys.getenv('sgbd_pwd')}")
dsn_consultation<-glue("PG:host={Sys.getenv('server')} port={Sys.getenv('port')} dbname='consultation' user={Sys.getenv('userid')} password={Sys.getenv('sgbd_pwd')}")

n_captage_p_r52<-st_read(dsn_consultation,query = "SELECT * FROM eau.n_captage_p_r52") # 1045 obs dont 869 eso et 175 esu
table_de_passage_bss_000= dbGetQuery(con_si_eau, "SELECT * FROM stations.table_de_passage_bss_000") # 851552 obs dont 59263 PDL
station_eso<-st_read(dsn_si_eau,query = "SELECT * FROM stations.station_eso_v0_9") # 1997 obs
nitrate_prelevement = dbGetQuery(con_si_eau, "SELECT * FROM nitrates.nitrate_prelevement_v0_9")


# n_captage_p_r52 et table_de_passage_bss_000 ------------------

# names(n_captage_p_r52)
# [1] "departement_captage"    "code_unite_gestion"     "nom_unite_gestion"      "code_captage"           "nom_captage"            "type_installation"     
# [7] "nature_eau_captage"     "usage_captage"          "date_debut_usage"       "etat_installation"      "date_etat_installation" "code_responsable_suivi"
# [13] "motif_abandon"          "code_bss"               "designation_bss"        "nom_commune_captage"    "code_commune_captage"   "coordonnee_x"          
# [19] "coordonnee_y"           "coordonnee_z"           "debit_moyen_m3j"        "the_geom"  

# code_captage (053000072)
# code_bss (02485X0510)

# names(table_de_passage_bss_000)
# [1] "nom_region"         "nom_departement"    "nom_commune"        "insee_commune"      "nouvel_identifiant" "ancien_identifiant" "indice"            
# [8] "designation"

# nouvel_identifiant (BSS000ZUPY)
# ancien_identifiant (03584X0135/F)
# indice (03584X0135)
# designation (F)

x<-semi_join(n_captage_p_r52,table_de_passage_bss_000,by=c("code_captage"="nouvel_identifiant")) # 0 obs
x2<-semi_join(n_captage_p_r52,table_de_passage_bss_000,by=c("code_captage"="ancien_identifiant")) # 0 obs
x3<-semi_join(n_captage_p_r52,table_de_passage_bss_000,by=c("code_captage"="indice")) # 0 obs

y<-semi_join(n_captage_p_r52,table_de_passage_bss_000,by=c("code_bss"="nouvel_identifiant")) # 17 obs
# unique(y$departement_captage)
# [1] "85" "44" "53" "72"
y2<-semi_join(n_captage_p_r52,table_de_passage_bss_000,by=c("code_bss"="ancien_identifiant")) # 0 obs
y3<-semi_join(n_captage_p_r52,table_de_passage_bss_000,by=c("code_bss"="indice")) # 781 obs
# unique(y3$departement_captage)
# [1] "44" "85" "53" "49" "72"

y4<-bind_rows(y,y3) # 798 obs

z<-filter(n_captage_p_r52,!is.na(code_bss)) # 835 obs

z2<-anti_join(z,y4 %>% st_drop_geometry()) # 37 obs


# station_eso et résultats précédents ------------

# names(station_eso)
# [1] "code_station"                 "code_sise_eaux"               "libelle_station"              "date_creation"                "source"                      
# [6] "code_masse_eau"               "code_eu_masse_eau"            "code_entite_hydro"            "code_troncon_hydro"           "code_commune"                
# [11] "code_sage"                    "code_bassin_versant"          "station_reference_pesticides" "captage_prioritaire_nitrates" "the_geom" 

# code_station (BSS000TRNA)
# code_sise_eaux (044000066)
# sauf une station : code_station (00000X0000), code_sise_eaux (049003611) et source (ARS)

a<-semi_join(station_eso,y4 %>% st_drop_geometry(),by=c("code_station"="code_bss")) # 17 obs
a2<-semi_join(station_eso,y4 %>% st_drop_geometry(),by=c("code_sise_eaux"="code_bss")) # 0 obs
a3<-semi_join(station_eso,y4 %>% st_drop_geometry(),by=c("code_sise_eaux"="code_captage")) # 20 obs
a4<-semi_join(station_eso,y4 %>% st_drop_geometry(),by=c("code_station"="code_captage")) # 0 obs

b<-semi_join(station_eso,z2 %>% st_drop_geometry(),by=c("code_station"="code_bss")) # 35 obs
b2<-anti_join(z2,b %>% st_drop_geometry(),by=c("code_bss"="code_station")) # 2 obs
# unique(b2$code_bss)
# [1] "BSS004AMCL" "03878X0121"


# nitrate_prelevement -------------------

# names(nitrate_prelevement)
# [1] "code_prelevement"     "code_intervenant"     "source"               "code_reseau"          "code_station"         "date_prelevement"    
# [7] "heure_prelevement"    "code_support"         "id_usage"             "id_prelevement_motif" "commentaire" 

# code_station
# source ADES : BSS000TRNA
# source Naïades : N71-410,N350510,M306999,03271654
# source ARS : 035000242

c<-semi_join(nitrate_prelevement,n_captage_p_r52 %>% filter(nature_eau_captage=="ESO"),
             by=c("code_station"="code_captage")) # 14117 obs, soit 584 code_station
c2<-semi_join(nitrate_prelevement,n_captage_p_r52%>% filter(nature_eau_captage=="ESO"),
              by=c("code_station"="code_bss")) # 20 obs, soit 6 code_station

d<-semi_join(nitrate_prelevement,station_eso,by=c("code_station"="code_station")) # 14439 obs, soit 629 code_station
d2<-semi_join(nitrate_prelevement,station_eso,by=c("code_station"="code_sise_eaux")) # 349 obs, soit 19 code_station

e<-semi_join(c,d) # 0 obs

